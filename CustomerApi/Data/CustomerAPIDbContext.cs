﻿using CustomerApi.Models;
using Microsoft.EntityFrameworkCore;

namespace CustomerApi.Data
{
    public class CustomerAPIDbContext : DbContext
    {
        //protected readonly IConfiguration Configuration;

        public CustomerAPIDbContext(DbContextOptions options) : base(options)
        {
        }
        //public CustomerAPIDbContext(IConfiguration configuration)
        //{
        //    Configuration = configuration;
        //}

        //protected override void OnConfiguring(DbContextOptionsBuilder options)
        //{
        //    options.UseNpgsql(Configuration.GetConnectionString("WebApiDatabase"));
        //}

        public DbSet<Customer> Customers { get; set; }
    }
}
