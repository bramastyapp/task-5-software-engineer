﻿using Microsoft.AspNetCore.Mvc;

namespace CustomerApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TodosController : Controller
    {
        private readonly string baseurl = " https://jsonplaceholder.typicode.com/todos";

        [HttpGet]
        public async Task<IActionResult> GetTodos()
        {
            try
            {
                using HttpClient http = new HttpClient();
                using HttpResponseMessage res = await http.GetAsync(baseurl);

                if (!res.IsSuccessStatusCode)
                {
                    return StatusCode((int)res.StatusCode, $"Failed to retrieve data from API. Status code: {res.StatusCode}");
                }

                using HttpContent content = res.Content;
                string data = await content.ReadAsStringAsync();

                if (data == null)
                {
                    return NotFound();
                }

                return Ok(data);
            }
            catch (HttpRequestException ex)
            {
                return StatusCode(500, $"An error occurred while calling the API: {ex.Message}");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }

        [HttpGet]
        [Route("{id:int}")]
        public async Task<IActionResult> GetTodo(int id)
        {

            try
            {
                using HttpClient http = new HttpClient();
                using HttpResponseMessage res = await http.GetAsync($"{baseurl}/{id}");

                if (!res.IsSuccessStatusCode)
                {
                    return StatusCode((int)res.StatusCode, $"Failed to retrieve data from API. Status code: {res.StatusCode}");
                }

                using HttpContent content = res.Content;
                string data = await content.ReadAsStringAsync();
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (HttpRequestException ex)
            {
                return StatusCode(500, $"An error occurred while calling the API: {ex.Message}");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }
    }
}
