﻿using CustomerApi.Data;
using CustomerApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace CustomerApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CustomersController : Controller
    {
        private readonly CustomerAPIDbContext dbContext;

        public CustomersController(CustomerAPIDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpGet]
        public async Task<IActionResult> GetCustomers()
        {
            try
            {
                var customers = await dbContext.Customers.ToListAsync();
                return Ok(customers);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Failed to get customers from database: {ex.Message}");
            }
        }

        [HttpGet]
        [Route("{id:guid}")]
        public async Task<IActionResult> GetCustomer([FromRoute] Guid id)
        {
            try
            {
                var customer = await dbContext.Customers.FindAsync(id);
                if(customer == null) 
                {
                    return NotFound();
                }

                return Ok(customer);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Failed to get customer from database: {ex.Message}");
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddCustomers(CustomersRequest addCustomerRequest) 
        {
            try
            {
                var customer = new Customer()
                {
                    Id = Guid.NewGuid(),
                    FirstName = addCustomerRequest.FirstName,
                    LastName = addCustomerRequest.LastName,
                    Email = addCustomerRequest.Email,
                    HomeAddress = addCustomerRequest.HomeAddress
                };

                await dbContext.Customers.AddAsync(customer);
                await dbContext.SaveChangesAsync();

                return Ok(customer);
            }
            catch (DbUpdateException ex)
            {
                return StatusCode(500, $"Failed to add customer to database: {ex.Message}");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred while adding customer: {ex.Message}");
            }
        }

        [HttpPut]
        [Route("{id:guid}")]
        public async Task<IActionResult> UpdateCustomer([FromRoute] Guid id, CustomersRequest updateCostomerRequest)
        {
            try
            {
                var customer = await dbContext.Customers.FindAsync(id);

                if (customer != null)
                {
                    customer.FirstName = updateCostomerRequest.FirstName;
                    customer.LastName = updateCostomerRequest.LastName;
                    customer.Email = updateCostomerRequest.Email;
                    customer.HomeAddress = updateCostomerRequest.HomeAddress;

                    await dbContext.SaveChangesAsync();

                    return Ok(customer);
                }

                return NotFound();
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Failed to update customer: {ex.Message}");
            }
        }

        [HttpDelete]
        [Route("{id:guid}")]

        public async Task<IActionResult> DeleteCustomer([FromRoute] Guid id)
        {
            try
            {
                var customer = await dbContext.Customers.FindAsync(id);

                if (customer != null)
                {
                    dbContext.Remove(customer);
                    await dbContext.SaveChangesAsync();
                    return Ok(customer);
                }

                return NotFound();
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred while deleting customer: {ex.Message}");
            }
        }
    }
}
